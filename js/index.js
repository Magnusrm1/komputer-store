


class App {

    constructor() {
        this.komputors = [
            { id: 0, name: "Macintosh", description: "This is a computer that you can do computer stuff on.", image: "../images/macintosh.jpg", price: 2000},
            { id: 1, name: "A Dell", description: "This is a computer that rolls in the deep.", image: "https://th.bing.com/th/id/OIP.tuIfoa2uOVnpLWklcQw3ZgHaE0?pid=Api&rs=1", price: 1700},
            { id: 2, name: "HP computer", description: "I have this type of computer, you should too i guess.", image: "https://store.hp.com/CanadaStore/Html/Merch/Images/c06234468_1750x1285.jpg", price: 1300},
            { id: 3, name: "Big Boi", description: "This computer has the power.", image: "https://i.ebayimg.com/images/g/1nwAAOSwzUVc-Ec5/s-l400.jpg", price: 2500}
        ];

        this.bankMoney = 200;
        this.salaryMoney = 0;
    
        this.elLoanButton = document.getElementById("loan-button");
        this.elWorkButton = document.getElementById("work-button");
        this.elBuyButton = document.getElementById("buy-button");
        this.elDepositButton = document.getElementById("deposit-salary-button");
        this.elSalaryDisplay = document.getElementById("salary-amount-label")
        this.elBalanceDisplay = document.getElementById("bank-balance-amount-label");
        this.elComputerSelect = document.getElementById("select-computer");
        this.elComputerName = document.getElementById("computer-name");
        this.elComputerDescription = document.getElementById("computer-description");
        this.elComputerPrice = document.getElementById("computer-price");
        this.elComputerImage = document.getElementById("computer-image");
        this.elBuyButton = document.getElementById("buy-button");

        return this;
    }
    
    init() {
        this.elSalaryDisplay.innerText = this.salaryMoney;
        this.elBalanceDisplay.innerText = this.bankMoney;

        this.elBuyButton.addEventListener("click", evt => this.clickBuyHandler(evt));
        this.elLoanButton.addEventListener("click", evt => this.clickLoanHandler(evt));
        this.elWorkButton.addEventListener("click", evt => this.clickWorkHandler(evt));
        this.elDepositButton.addEventListener("click", evt => this.clickDepositHandler(evt));
        this.elComputerSelect.addEventListener("change", evt => this.fillComputerInfo(evt));

        this.addComputerOptions();
        this.fillComputerInfo();
    }

    addComputerOptions() {
        for (let i = 0; i < this.komputors.length; i++) {
            const elOption = document.createElement("option");
            elOption.textContent = this.komputors[i].name;
            elOption.value = this.komputors[i].id;
            this.elComputerSelect.appendChild(elOption);
        }
    }

    fillComputerInfo() {
        const computerId = this.elComputerSelect.options[this.elComputerSelect.selectedIndex].value;
        let computer = this.komputors.find(computer => computer.id == computerId );

        this.elComputerImage.src = computer.image;
        this.elComputerName.innerText = computer.name;
        this.elComputerPrice.innerText = computer.price;
        this.elComputerDescription.innerText = computer.description;
    }

    clickWorkHandler() {
        this.salaryMoney += 100;
        this.elSalaryDisplay.innerText = this.salaryMoney;
    }

    clickDepositHandler() {
        this.bankMoney += this.salaryMoney;
        this.salaryMoney = 0;
        this.elSalaryDisplay.innerText = this.salaryMoney;
        this.elBalanceDisplay.innerText = this.bankMoney;
    }

    clickLoanHandler() {
        let loanAmount = parseInt(window.prompt("Enter loan amount:", "0"));

        if (!(typeof loanAmount == 'number')) {
            window.alert("Please give a non decimal numerical value when requesting a loan.");
        }
        else if(loanAmount >= this.bankMoney * 2) {
            window.alert("You cannot get a loan over twice the size of your bank.");
        } else {
            this.bankMoney += loanAmount;
            this.elBalanceDisplay.innerText = this.bankMoney;
        }
    }

    clickBuyHandler() {
        let computerId = this.elComputerSelect.options[this.elComputerSelect.selectedIndex].value;
        let computer = this.komputors.find(computer => computer.id == computerId );

        if (this.bankMoney >= computer.price) {
            this.bankMoney -= computer.price;
            this.elBalanceDisplay.innerText = this.bankMoney;
            window.alert("The komputor is now yours!");
        } else {
            window.alert("You cant afford this computer.");
        }
    }
}

new App().init();